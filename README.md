# SCRIPT PARA KEEPA

_Este script de Python le permite interactuar con la API en Keepa para consultar información e historial de productos de Amazon. También contiene un módulo de trazado para permitir el trazado de un producto._

## Comenzando 🚀

_Este proyecto fue creado y testeado en un entorno virtual creado por **Anaconda**, en alternativa a este se puede utilizar **Virtualenv**, la version de python utilizada y recomendada es **Python 3.6**_

### Conda
En caso de que quieras ejecutar el programa en un entorno virtual con Anaconda, debes seguir la siguiente instruccion:

```
conda create -n <nombre_del_entorno>
```
luego activas el entorno ejecutando:

```
conda activate <nombre_del_entorno>
```

### Virtualenv

En caso de que quieras usar Virtualenv deberas seguir las siguientes instrucciones:
En este caso asumiremos que estas usando un entorno con base Linux (ubuntu en su version 20), en la mayoria de distribuciones Linux tiene preinstalado Python 2 pero no es la version que necesitamos, para ello necesitamos instalar python3 y sus dependencias principales:

```
apt install python3 python3-pip virtualenv
```
luego de ello procedemos a ubicar el directorio principal de python3:
```
which python3
```

este comando respondera algo como lo siguiente:
```
/usr/bin/python3
```
entonces procederemos a realizar el comando para crear el entorno:
```
virtualenv -p /usr/bin/python3 <nombre_del_entorno>
```
para finalizar, debemos iniciar el entorno, para eso lanzamos el siguiente comando:
```
source <nombre_del_entorno>/bin/activate
```

### Pre-requisitos 📋
_para utilizar el programa necesitas instalar las dependencias:_

```
aiohttp==3.7.3
numpy==1.19.5
pandas==1.1.5
psycopg2-binary==2.8.6
requests==2.25.1
SQLAlchemy==1.3.22
tqdm==4.56.0
```

para instalarlas solo necesitas ejecutar el siguiente comando:

```
pip install -r requirements.txt
```

## .env Configuraciones de entorno
hay un archivo llamado example.env, deberas hacer una copia y renombrarlo *.env* dentro podras configurar el acceso a base de datos y tambien la api de keepa que se utilizara para consultar a la api.

## Lista de asines
hay un archivo llamado asins en el que colocan en lista los productos (asines) a consultar, estos tienen que estar en una lista como la siguiente:
```
B00S29G7BC
B00S82BAX8
B00SBYGKN8
B00SQG1DWY
B00SW10VZ8
B00SYORA6G
B00SYXSESA
B00T1R8UVO
B00T366JBG
B00T36RIIY
B00TM2BSEY
```


## Ejecutando las pruebas ⚙️
Para probar la funcionalidad del programa y sus dependencias bastara solo con ejecutar:

```
python main.py
```
_el comando anterior devolvera una lista de opciones que usted puede utilizar al momento de colocar en produccion el script, ademas de eso realizara un pre_ping a la base de datos asegurando la conexion y alive de la misma._

### Opciones de Keepa Update 🔩
_En este punto es necesario comprender que opciones son necesarias y cuales son opcionales, para saber, el programa tiene ya un sistema de ayuda, por lo que solo necesita ejecutar el siguiente comando para obtener una lista y la descripcion de las opciones disponibles_
```
python main.py -h
```
las opciones serian las siguientes:

```
-nt NUMBER_THREADS, --number_threads NUMBER_THREADS
                        cuantos hilos (proceso simultaneos) va a utilizar el
                        proceso, mientras mas alto el numero mas alto es el
                        consumo de recursos.
-f FILE, --file FILE  Indico cual será el archivo que voy a leer para
                    obtener los asines, debe ser una lista plana con un
                    asin por linea
-sf STEPS_FILE, --steps_file STEPS_FILE
                    se le indica la cantidad de asines que contendra cada
                    archivo al momento de ser dividido el archivo de
                    asines original
-v VARIANTES, --variantes VARIANTES
                    Este parametro defini si quieres que los asines vengan
                    con las variantes o no
-w WORKSPACE, --workspace WORKSPACE
                    Cuando llames a este parametro se crearan las tablas
                    en la base de datos si no existen
```
_la unica opcion que es opcional es **WORKSPACE**_

## Despliegue 📦
Ahora ejecutaremos el comando para ponerlo en produccion, para ello debemos tener en cuanta el numero de asines a analizar y el numero de hilos a correr, por ejemplo la forma mas optima para 10000 asines (basandose en la prueba en el momento de desarrollo) es de la siguiente forma:

```
python main.py -nt 20 -f asins -sf 100 -w true -v true

```
## Construido con 🛠️
_estas son las principales herramientas que se utilizaron en el proyecto_


- [tqdm](https://tqdm.github.io) - Instantly make your loops show a smart progress meter
- [AIOHTTP](https://docs.aiohttp.org/en/stable/) - Asynchronous HTTP Client/Server for asyncio and Python.
- [NumPy](https://numpy.org/doc/stable/) - The fundamental package for scientific computing with Python.
- [Pandas](https://pandas.pydata.org/docs/) - pandas is an open source, BSD-licensed library providing high-performance, easy-to-use data structures and data analysis tools for the Python programming language.
- [Psycopg2](https://www.psycopg.org/docs/) - Psycopg is the most popular PostgreSQL adapter for the Python programming language. Its core is a complete implementation of the Python DB API 2.0 specifications. Several extensions allow access to many of the features offered by PostgreSQL.
- [SQLAlchemy](https://docs.sqlalchemy.org/en/13/)

## Wiki 📖
Puedes averiguar mas sobre la api de keepa en el siguiente enlace https://keepa.com/#!api

## Versionado 📌
teniendo en cuenta el script anterior, este codigo lleva como version la siguiente:
- 3.13
correspondiente al numero de updates hasta el momento.

## Autores ✒️
- Ivan Montes - Trabajo inicial y documentacion [iandres.montes](https://gitlab.com/iandres.montes)




