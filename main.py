from models.offers import Offers
from models.products import Products
from models.variations import Variations
from config.helpers import asins
from config import engine, Base
import argparse
from config import process
import sys

def create_workspace():
    Base.metadata.create_all(engine)



parser = argparse.ArgumentParser(description='Keepa update.')

parser.add_argument('-sf', '--steps_file', type=int, help='se le indica la cantidad de asines que contendra cada archivo al momento de ser dividido el archivo de asines original',required=True)

parser.add_argument('-nt', '--number_threads', type=int, help='cuantos hilos (proceso simultaneos) va a utilizar el proceso, mientras mas alto el numero mas alto es el consumo de recursos.', required=True)

parser.add_argument('-v', '--variantes', type=bool, help='Este parametro defini si quieres que los asines vengan con las variantes o no')
parser.add_argument('-w', '--workspace', type=bool, help='Cuando llames a este parametro se crearan las tablas en la base de datos si no existen')

args = parser.parse_args()

if args.workspace:
    create_workspace()
# print(args)

variantes = True if args.variantes else False

process(args.number_threads, variantes, steps=asins().writeFile(args.steps_file))
