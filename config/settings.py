from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import configparser

config = configparser.ConfigParser()
config.read('.env')
configurate = config['CONFIGURATION']
keepa = config['KEEPA']

def settings():
    _json = {
        "keepa":{
            "key":keepa.get('KEEPA_KEY'),
            "refill_rate":keepa.get('REFILL_RATE'),
            "domain":keepa.get('DOMAIN'),
            "number_threads":200
        },
        'app':configurate
    }
    return _json



engine = create_engine('postgresql+psycopg2://'+configurate.get('DB_USERNAME')+':'+configurate.get('DB_PASSWORD')+'@'+configurate.get('DB_HOST')+':'+configurate.get('DB_PORT')+'/'+configurate.get('DB_DATABASE'), pool_pre_ping = True )
Session = sessionmaker(bind=engine)
session = Session()
Base = declarative_base()