from config import settings, session, configurate
from models.products import Products
from models.variations import Variations
from models.offers import Offers
from os import path, walk
import requests as r
from config.keepa import Keepa
from sqlalchemy import text
import requests
import os, sys 
import glob
import time
import threading
from tqdm import tqdm
import re
import psycopg2
from datetime import datetime
import ast

connection = psycopg2.connect(user=configurate.get('DB_USERNAME'),
                                            password=configurate.get('DB_PASSWORD'),
                                            host=configurate.get('DB_HOST'),
                                            port=configurate.get('DB_PORT'),
                                            database=configurate.get('DB_DATABASE'))

connection.set_session(readonly=False, autocommit=True)

keepa_settings = settings()['keepa']

class process:
    def __init__(self, steps, number_threads, variantes, session=session):
        #Inicializacion de variables
        self._step_files = steps
        self._number_threads = number_threads
        self._variantes = variantes

        self._key = keepa_settings.get('key')
        self._api = Keepa(keepa_settings.get('key'))
        self._thread_pool = list()
        self._tokensleft = 0
        self._refillRate = keepa_settings.get('refill_rate')
        self._running = True
        self._total_products = 0
        self._total_offers = 0
        self._total_variations = 0
        self._remove = []
        self._cursor = connection.cursor()

        self.strip_files()
        self.init_process(session)

    def newfilling(self):
        status_url = "https://api.keepa.com/token?key=" + self._key
        result = r.get(status_url).json()
        self._tokensleft = int(result.get("tokensLeft"))
        self._refillRate = int(result.get("refillRate"))
        time.sleep((int(result.get("refillIn"))) / 100)
        self._tokensleft += self._refillRate
        # print(self._tokensleft)
    
    def killer(self):
        try:
            while self._running:
                if len(self._remove) == 0:
                    continue
                else:
                    for thread_index, thread in enumerate(self._thread_pool):
                        if thread.getName() in self._remove:
                            # CIERRO EL HILO Y LO ELIMINO DEL POOL.
                            self._remove.pop(self._remove.index(thread.getName()))
                            self._thread_pool[thread_index].join()
                            self._thread_pool.pop(thread_index)
        except Exception as err:
            pass

    def refilling(self):
        status_url = "https://api.keepa.com/token?key=" + self._key
        result = r.get(status_url).json()
        self._tokensleft = int(result.get("tokensLeft"))
        self._refillRate = int(result.get("refillRate"))
        time.sleep((int(result.get("refillIn")) + 100) / 1000)
        self._tokensleft += self._refillRate
        while self._running:
            try:
                status_url = "https://api.keepa.com/token?key=" + self._key
                result = r.get(status_url).json()
                self._tokensleft = int(result.get("tokensLeft"))
                self._refillRate = int(result.get("refillRate"))
                time.sleep((int(result.get("refillIn")) + 100) / 1000)
                self._tokensleft += self._refillRate
                if self._refillRate * 60 > self._tokensleft:
                    self._tokensleft = self._refillRate * 60
                # print("Refiller:     Refilled", self._refillRate, "new tolensLeft:", self._tokensleft)
            except:
                pass  
    def insert_database(self,base_query, session, lista, nombre):
        try:
            to_insert = None
            if len(lista) <= 10000:
                sql_command = base_query + (', '.join(lista))
                to_insert = sql_command
                # session.begin_nested()
                self._cursor.execute(sql_command)
                # connection.commit()
            else:
                for i in range(0, len(lista), 5000):
                    sql_command = base_query + (', '.join(lista[i:i+5000]))
                    to_insert = sql_command
                    # session.begin_nested()
                    self._cursor.execute(sql_command)
                    # self._connection.commit()
        except Exception as err:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            e = open("./errors.log", 'a+', encoding='utf-8')
            e.write(str(exc_type) + '' + str(fname) + '' + str(exc_tb.tb_lineno)+ ' ' + str(err) + "\n" + "\n")
            e.close()
            f = open("./insert_errors.log", 'a+', encoding='utf-8')
            f.write(str(to_insert)+"\n\n-----------------------------------------------------------------------------\n")
            f.close()
            time.sleep(0.3)

        # finally:
        #     session.close()  # optional, depends on use case


    # def strip_files(self): 
    #     if path.isfile(self._path):
    #         with open(self._path) as fin:
    #             [os.remove(f) for f in glob.glob('./resources/*')]
    #             fout = open("./resources/output_0.txt","a")
    #             for i,line in enumerate(fin):
    #                 fout.writelines(line)
    #                 if (i+1) % self._step_files == 0:
    #                     fout.close()
    #                     fout = open("./resources/output_%d.txt"%(i/self._step_files+1),"a")
    #             fout.close()
    #     else:
    #         print('*** El path es un folder o no existe ***')

    def get_offers(self, asin, offers):
        def parse(item):
            new_item = re.sub(r'[^\w.,$ ]', '', item)
            return new_item
        try:
            if offers:
                offers = offers[-1]
                offerCSV = offers['offerCSV']
                recent_price = offerCSV[len(offerCSV) - 2]
                recent_shipping_cost = offerCSV[len(offerCSV) - 1]
                
                offer = """
                        ('{asin}', {offerId}, {lastSeen}, '{sellerId}', {isPrime}, {isFBA}, {isMAP}, {isShippable}, {isAddonItem}, {isPreorder}, {isWarehouseDeal}, {isScam}, {shipsFromChina}, {isAmazon}, {isPrimeExcl}, {condition}, '{conditionComment}', {recent_price}, {recent_shipping_cost})
                """.format(
                        asin=asin,
                        offerId= offers.get('offerId') if 'offerId' in offers else None, 
                        lastSeen = offers.get('lastSeen') if 'lastSeen' in offers else None,
                        sellerId = offers.get('sellerId') if 'sellerId' in offers else None,
                        isPrime = offers.get('isPrime') if 'isPrime' in offers else None,
                        isFBA = offers.get('isFBA') if 'isFBA' in offers else None,
                        isMAP = offers.get('isMAP') if 'isMAP' in offers else None,
                        isShippable= offers.get('isShippable') if 'isShippable' in offers else None,
                        isAddonItem= offers.get('isAddonItem') if 'isAddonItem' in offers else None,
                        isPreorder= offers.get('isPreorder') if 'isPreorder' in offers else None,
                        isWarehouseDeal= offers.get('isWarehouseDeal') if 'isWarehouseDeal' in offers else None,
                        isScam= offers.get('isScam') if 'isScam' in offers else None,
                        shipsFromChina= offers.get('shipsFromChina') if 'shipsFromChina' in offers else None,
                        isAmazon= offers.get('isAmazon') if 'isAmazon' in offers else None,
                        isPrimeExcl= offers.get('isPrimeExcl') if 'isPrimeExcl' in offers else None,
                        condition= offers.get('condition') if 'condition' in offers else None,
                        conditionComment=parse( offers.get('conditionComment') )if 'conditionComment' in offers else None,
                        recent_price= recent_price,
                        recent_shipping_cost= recent_shipping_cost
                )

                return offer
            else:
                pass
        except Exception as error:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno, error)

    def get_variations(self, asin,variations):
        def parse(item):
            new_item = re.sub(r'[^\w.,$ ]', '', item)
            return new_item
        
        try:
            lista = []
            if variations:
                for item in variations:
                    variation_name = item['attributes'][0]['value']
                    variation_dimension = item['attributes'][0]['dimension']

                    if len(item['attributes']) == 2:
                        attribute_asin = item['asin']
                        attribute_dimension = item['attributes'][1]['dimension']
                        attribute_value = item['attributes'][1]['value']
                    else:
                        attribute_asin = None
                        attribute_dimension = None
                        attribute_value = None
                        
                    query = """('{parent_asin}', '{variation_name}', '{variation_dimension}', '{attribute_asin}', '{attribute_dimension}', '{attribute_value}')""".format(
                        parent_asin = asin, 
                        variation_name = parse(variation_name), 
                        variation_dimension = parse(variation_dimension), 
                        attribute_asin = attribute_asin, 
                        attribute_dimension = parse(str(attribute_dimension)) if attribute_dimension else None, 
                        attribute_value = parse(str(attribute_value)) if attribute_value else None
                    )
                    lista.append(query)


                return lista
            else:
                pass
        except Exception as err:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno, err)
            

    def update_product(self, p):
        def parse(item):
            new_item = re.sub(r'[^\w.,$ ]', '', item)
            return new_item
        try:
            status = 1

            if p.get("productType") != 0 and p.get("availabilityAmazon") in [2, 3]:
                status = 0

            amazon_prices = p.get("csv")[0]
            new_prices = p.get("csv")[1]
            
            
            if amazon_prices is not None and amazon_prices[-1] > 0:
                price = amazon_prices[-1] / 100
            elif new_prices is not None and new_prices[-1] > 0:
                price = new_prices[-1] / 100
            else:
                price = -1
                status = 0

            if p.get("categories") is not None and len(p.get("categories")) > 0:
                category = p.get("categories")[len(p.get("categories")) - 1]
            else:
                category = p.get("rootCategory")
         

            dollar_price = 3040
            to_lb = 0.00220462262
            to_inch = 0.03937
            package_weight =  float("{:.5f}".format((int(p.get("packageWeight")) * to_lb)))
            if price == -1:
                cop_price = -1
            else:
                ship_cost = 7.5 if package_weight < 5 else package_weight * 1.5
                cop_price = ((price * 1.07) + ship_cost) * dollar_price

            new_poduct = """({category_id}, '{imagescsv}', {user_id}, {store_id}, '{color}', '{title}', '{description}', '{information}', '{brand}', '{asin}', '{sku}','{upc}','{ean}', {usd}, {old_price}, {discount_percentage}, {discount}, {earnings_percentage}, {stock}, {weight}, {height}, {length}, {width}, '{size}', '{type}', '{product_group}', '{genre}', '{model}', '{edition}', '{platform}', '{format}', {is_prime}, {is_adult_product}, {package_height}, {package_length}, {package_width}, {package_weight}, {package_quantity}, '{features}', '{category_tree}', {status},{productType},{lastUpdate}, {lastRatingUpdate},{lastPriceChange},{availabilityAmazon},'{csv}')""".format(
                                                category_id=category,
                                                imagescsv=p.get('imagesCSV'),
                                                user_id=16,
                                                store_id=0,
                                                color=parse(str(p.get("color"))),
                                                title=parse(p.get("title")) if p.get("title") else '',
                                                description=parse(p.get("description")) if p.get("description") else '',
                                                information=parse("\n".join(p.get("features"))) if p.get("features") is not None else 'NULL',
                                                brand=parse(p.get("brand")) if p.get("brand") else '',
                                                asin= p.get("asin"),
                                                sku= 'NULL',
                                                upc= parse(str(p.get("upcList"))),
                                                ean= parse(str(p.get("eanList"))),
                                                usd= int(price),
                                                old_price= float("{:.2f}".format(cop_price)),
                                                discount_percentage= 0,
                                                discount= 0,
                                                earnings_percentage= 0,
                                                stock= 5,
                                                weight= float("{:.5f}".format(int(p.get("itemWeight")) * to_lb)),
                                                height=  float("{:.5f}".format(int(p.get("itemHeight")) * to_inch)),
                                                length=  float("{:.5f}".format(int(p.get("itemLength")) * to_inch)),
                                                width=  float("{:.5f}".format(int(p.get("itemWidth")) * to_inch)),
                                                size= parse(p.get("size")[0:16]) if p.get("size") is not None else 'NULL',
                                                type= parse(p.get("type")) if p.get("type") else 'NULL',
                                                product_group= p.get("productGroup"),
                                                genre= parse(p.get("genre")) if p.get("genre") else 'NULL',
                                                model= parse(p.get("model")) if p.get("model") else 'NULL',
                                                edition= parse(p.get("edition")) if p.get("edition") else 'NULL',
                                                platform= parse(p.get("platform")) if p.get("platform") else 'NULL',
                                                format= parse(p.get("format")) if p.get("format") else 'NULL',
                                                is_prime= 1,
                                                is_adult_product= 1 if p.get("isAdultProduct") else 0,
                                                package_height=  float("{:.5f}".format(int(p.get("packageHeight")) * to_inch)),
                                                package_length=  float("{:.5f}".format(int(p.get("packageLength")) * to_inch)),
                                                package_width=  float("{:.5f}".format(int(p.get("packageWidth")) * to_inch)),
                                                package_weight= package_weight,
                                                package_quantity= p.get("packageQuantity"),
                                                features= parse("\n".join(p.get("features"))) if p.get("features") is not None else "",
                                                category_tree= None,
                                                status= status,
                                                productType = p.get("productType"),
                                                lastUpdate = p.get("lastUpdate"),
                                                lastRatingUpdate = p.get("lastRatingUpdate"),
                                                lastPriceChange = p.get("lastPriceChange"),
                                                availabilityAmazon = p.get("availabilityAmazon"),
                                                csv = p.get("csv")[16]
                                                
                                            

                                )
            
            new_offers = self.get_offers(p.get("asin"),p.get('offers'))
            
            if self._variantes:
                new_variations = self.get_variations(p.get("asin"),p.get('variations'))
            else:
                new_variations = []

            _json = {"new_poduct": new_poduct, "new_offers":new_offers, "new_variations":new_variations}
            
            return _json

        except Exception as error:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno, error)

    def parse_products(self, session,lista):
        ret_products = []
        ret_offers = []
        ret_variations = []
        for product in lista:
            retorno = self.update_product(product)
            if retorno['new_poduct']:
                ret_products.append(retorno['new_poduct'])
            if retorno['new_offers']:
                ret_offers.append(retorno['new_offers'])
            if retorno['new_variations']:
                ret_variations = ret_variations + retorno['new_variations']

        if ret_products:
            base_query = """INSERT INTO top1_10k_industria (category_id, imagescsv, user_id, store_id, color, title, description, information, brand, asin, sku, upc, ean, usd, old_price, discount_percentage, discount, earnings_percentage, stock, weight, height, length, width, "size", "type", product_group, genre, model, edition, platform, format, is_prime, is_adult_product, package_height, package_length, package_width, package_weight, package_quantity, features, category_tree, status, productType, lastUpdate, lastRatingUpdate, lastPriceChange, availabilityAmazon, csv) VALUES"""
            products_thread = threading.Thread(target=self.insert_database, args=(base_query,session,ret_products,'ret_products'))  # CREO EL HILO.
            products_thread.start() 
            self._total_products = self._total_products + len(ret_products)
            products_thread.join()
            del ret_products

        if ret_offers:
            base_query = """INSERT INTO top1_10k_industria_offers (asin, "offerId", "lastSeen", "sellerId", "isPrime", "isFBA", "isMAP", "isShippable", "isAddonItem", "isPreorder", "isWarehouseDeal", "isScam", "shipsFromChina", "isAmazon", "isPrimeExcl", "condition", "conditionComment", recent_price, recent_shipping_cost) VALUES"""
            offers_thread = threading.Thread(target=self.insert_database, args=(base_query,session,ret_offers,'ret_offers'))  # CREO EL HILO.
            offers_thread.start() 
            self._total_offers = self._total_offers + len(ret_offers)
            offers_thread.join()
            del ret_offers 

        if ret_variations:
            base_query = """ INSERT INTO top1_10k_industria_variantes (parent_asin, variation_name, variation_dimension, attribute_asin, attribute_dimension, attribute_value) VALUES """
            variations_thread = threading.Thread(target=self.insert_database, args=(base_query,session,ret_variations,'ret_variations'))  # CREO EL HILO.
            variations_thread.start() 
            self._total_variations = self._total_variations + len(ret_variations)
            variations_thread.join() 
            del ret_variations


    def get_products(self,session,asins):
        try:
            products = self._api.query(asins, offers=20)
            self.parse_products(session,products)
        except Exception as err:
            print('get_products: ',err)

    def init_process(self, session):
        start_time = time.time()
        try:
            refiller = threading.Thread(target=self.refilling)
            refiller.start()

            killer = threading.Thread(target=self.killer)
            killer.start()

            _, _, filenames = next(walk('./resources/'))
            for file in tqdm(filenames):
                with open('./resources/{}'.format(file), 'r') as asins_file:
                    try:
                        while self._tokensleft < ((int(self._step_files)*6) + 10000) :
                            print("Esperando tokens {}".format(self._tokensleft), end="\r")
                            time.sleep(0.1)

                        while len(self._thread_pool) >= self._number_threads:
                            print("Numero de tareas excedidas >> tareas: {} - hilos permitidos {}".format(len(self._thread_pool),self._number_threads), end="\r")
                            time.sleep(0.1)
                            
                        asins = [x.strip() for x in asins_file]
                        if asins:
                            # self.get_products(session,['B06WVFZK1C','B00BT23TBU'])
                            tmp_thread = threading.Thread(target=self.get_products, args=(session,asins,))  # CREO EL HILO.
                            tmp_thread.setName(file)  # AGREGO EL IDENTIFICADOR DEL HILO
                            self._thread_pool.append(tmp_thread)  # AGREGO EL HILO AL POOL
                            tmp_thread.start()  # INICIO LA EJECUCIÓN DEL HILO CREADO
                        
                        
                        del asins
                        asins_file.close()
                        self._remove.append(file)
                        # self.newfilling()
                    except Exception as error:
                        exc_type, exc_obj, exc_tb = sys.exc_info()
                        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                        print(exc_type, fname, exc_tb.tb_lineno, error)

            print("Finalizando tareas pendientes")
            while len(self._thread_pool) > 0:
                print("Hilos pendientes {}".format(len(self._thread_pool)), end="\r")
                try:
                    for thread_index, thread in enumerate(self._thread_pool):
                        if thread.getName() in self._remove:
                            # CIERRO EL HILO Y LO ELIMINO DEL POOL.
                            thread.join()
                            self._remove.pop(self._remove.index(thread.getName()))
                            self._thread_pool[thread_index].join()
                            self._thread_pool.pop(thread_index)
                except Exception as err:
                    pass
            
            self._running = False
            refiller.join()
            killer.join()

            print('\n\n',"--- %s seconds ---" % (time.time() - start_time))
            print("Total products: ",self._total_products,"\n",
                    "Total offers: ",self._total_offers,'\n', 
                    "Total variations: ",self._total_variations
                    # "Total rechazados: ", len(lista)
                    )

        except Exception as error:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno, error)
        
        finally:
            rechazados = open('insert_errors.log', 'r', encoding='utf-8')
            
            lista = []
            for item in rechazados:
                if '-----------------------------------------------------------------------------' in item and len(item) < 1:
                    break
                lista.append(item)
                self._cursor.execute(sql_command)
                time.sleep(5)

