import requests as r
from config import settings, configurate, session
from models.products_production import Products
import time, os
import psycopg2
import glob

connection = psycopg2.connect(user=configurate.get('DB_USERNAME'),
                                            password=configurate.get('DB_PASSWORD'),
                                            host=configurate.get('DB_HOST'),
                                            port=configurate.get('DB_PORT'),
                                            database=configurate.get('DB_DATABASE'))

connection.set_session(readonly=False, autocommit=True)

keepa = settings()['keepa']

def refilling(running=True):
    status_url = "https://api.keepa.com/token?key=" + keepa['key']
    result = r.get(status_url).json()
    tokensLeft = int(result.get("tokensLeft"))
    refillRate = int(result.get("refillRate"))
    time.sleep((int(result.get("refillIn")) + 100) / 1000)
    tokensLeft += refillRate
    while running:
        try:
            print(tokensLeft)
            status_url = "https://api.keepa.com/token?key=" + API_KEY
            result = r.get(status_url).json()
            tokensLeft = int(result.get("tokensLeft"))
            refillRate = int(result.get("refillRate"))
            time.sleep((int(result.get("refillIn")) + 100) / 1000)
            tokensLeft += refillRate
            if refillRate * 60 > tokensLeft:
                tokensLeft = refillRate * 60
            #print("Refiller:     Refilled", refillRate, "new tolensLeft:", tokensLeft)
        except:
            pass    

class asins:
    def __init__(self): 
        self._cursor = connection.cursor()
        self.deleteFile()

    def writeFile(self,steps_file):
        total = session.query(Products).filter_by(vitrina=True).count()
        aux = 0
        steps = steps_file
        count=0
        while aux < (total+steps): 
            file = open('./resources/output_{}'.format(count),'w')
            asins = session.query(Products).limit(steps).offset(aux).all()
            for item in asins:
                file.writelines('{}\n'.format(item.asin))
            aux += steps
            count += 1
        
        session.close()
        return steps_file


    def deleteFile(self):
        [os.remove(f) for f in glob.glob('./asins/*')]

        # f = open('asins', 'r+')
        # f.truncate(0)