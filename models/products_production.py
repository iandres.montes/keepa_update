from config.settings import Base
from sqlalchemy import Column, Integer, String, Float, BigInteger, Boolean

class Products(Base):
    __tablename__ = 'products'
    product_id= Column(Integer, primary_key=True)
    asin= Column(String(250))
    vitrina= Column(Boolean)
    
    def __repr__(self):
        return f'Producto({self.product_id})'
