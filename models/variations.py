from config.settings import Base
from sqlalchemy import Column, Integer, String

class Variations(Base):
    __tablename__ = 'fldsmdfr_products'
    id = Column(Integer, primary_key=True)
    parent_asin = Column(String(12250), nullable=False)
    variation_name = Column(String(12250), nullable=False)
    variation_dimension = Column(String(12250))
    attribute_asin = Column(String(12250)) 
    attribute_dimension = Column(String(12250)) 
    attribute_value = Column(String(12250)) 

    def __init__(self, _json):
        self.parent_asin = _json['parent_asin']
        self.variation_name = _json['variation_name']
        self.variation_dimension = _json['variation_dimension'] if _json['variation_dimension'] else None
        self.attribute_asin = _json['attribute_asin'] if _json['attribute_asin'] else None
        self.attribute_dimension = _json['attribute_dimension'] if _json['attribute_dimension'] else None
        self.attribute_value = _json['attribute_value'] if _json['attribute_value'] else None
    def __repr__(self):
        return f'Variations({self.parent_asin})'
    def __str__(self):
        return self.parent_asin