from config.settings import Base
from sqlalchemy import Column, Integer, String, Float, DateTime, Boolean, BigInteger

class Offers(Base):
    __tablename__ = 'fldsmdfr_products'

    id = Column(Integer, primary_key=True)
    asin = Column(String(250), nullable=False)
    offerId = Column(Integer)
    lastSeen = Column(Integer)
    sellerId = Column(String(4500))
    isPrime = Column(Boolean)
    isFBA = Column(Boolean)
    isMAP = Column(Boolean)
    isShippable = Column(Boolean)
    isAddonItem = Column(Boolean)
    isPreorder = Column(Boolean)
    isWarehouseDeal = Column(Boolean)
    isScam = Column(Boolean)
    shipsFromChina = Column(Boolean)
    isAmazon = Column(Boolean)
    isPrimeExcl = Column(Boolean)
    condition = Column(Integer)
    conditionComment = Column(String(4500))
    recent_price = Column(Integer)
    recent_shipping_cost = Column(Integer)


    def __init__(self,asin,offerId,lastSeen ,sellerId ,isPrime ,isFBA ,isMAP ,isShippable,isAddonItem,isPreorder,isWarehouseDeal,isScam,shipsFromChina,isAmazon,isPrimeExcl,condition,conditionComment,recent_price,recent_shipping_cost):
        self.asin = asin
        self.offerId = offerId 
        self.lastSeen = lastSeen  
        self.sellerId = sellerId  
        self.isPrime = isPrime  
        self.isFBA = isFBA  
        self.isMAP = isMAP  
        self.isShippable = isShippable 
        self.isAddonItem = isAddonItem 
        self.isPreorder = isPreorder 
        self.isWarehouseDeal = isWarehouseDeal 
        self.isScam = isScam 
        self.shipsFromChina = shipsFromChina 
        self.isAmazon = isAmazon 
        self.isPrimeExcl = isPrimeExcl 
        self.condition = condition 
        self.conditionComment = conditionComment 
        self.recent_price = recent_price 
        self.recent_shipping_cost = recent_shipping_cost 
    def __repr__(self):
        return f'Offer({self.asin})'
    def __str__(self):
        return self.asin