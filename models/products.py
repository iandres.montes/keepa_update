from config.settings import Base
from sqlalchemy import Column, Integer, String, Float, BigInteger

class Products(Base):
    __tablename__ = 'fldsmdfr_products'
    product_id= Column(Integer, primary_key=True)
    category_id= Column(BigInteger)
    imagescsv= Column(String(12500))
    user_id= Column(Integer)
    store_id= Column(Integer)
    color= Column(String(12500))
    title= Column(String(12500))
    description= Column(String(14500))
    information= Column(String(14500))
    brand= Column(String(5000))
    asin= Column(String(5000))
    sku= Column(String(5000))
    upc= Column(String(5000))
    ean= Column(String(5000))
    usd= Column(BigInteger)
    old_price= Column(BigInteger)
    discount_percentage= Column(Integer)
    discount= Column(Integer)
    earnings_percentage= Column(Integer)
    stock= Column(Integer)
    weight= Column(Float)
    height= Column(Float)
    length= Column(Float)
    width= Column(Float)
    size= Column(String(12500))
    type= Column(String(12500))
    product_group= Column(String(12500))
    genre= Column(String(12500))
    model= Column(String(12500))
    edition= Column(String(12500))
    platform= Column(String(12500))
    format= Column(String(12500))
    is_prime= Column(Integer)
    is_adult_product= Column(Integer)
    package_height= Column(Float)
    package_length= Column(Float)
    package_width= Column(Float)
    package_weight= Column(Integer)
    package_quantity= Column(Integer)
    features= Column(String(12500))
    category_tree= Column(String(12500))
    status= Column(Integer)
    created_since= Column(String(12500))
    updated_since= Column(String(12500))
    new_cat_id= Column(Integer)
    bestseller= Column(Integer)
    relevance= Column(Integer)
    sales_accountant= Column(Integer)
    producttype= Column(Integer)
    lastupdate=Column(String(12500))
    lastratingupdate= Column(String(12500))
    lastpricechange= Column(String(12500))
    availabilityamazon= Column(Integer)
    csv= Column(String(10485760))
   
    

    def __init__(self,category_id,user_id,store_id,color,title,description,information,brand,asin,sku,upc,ean,usd,old_price,discount_percentage,discount,earnings_percentage,stock,weight,height,length,width,size,type,product_group,genre,model,edition,platform,format,is_prime,is_adult_product,package_height,package_length,package_width,package_weight,package_quantity,features,category_tree,status, productType, lastupdate, lastratingupdate, lastpricechange, availabilityamazon, csv):
        self.category_id = category_id
        self.user_id = user_id
        self.store_id = store_id
        self.color = color
        self.title = title
        self.description = description
        self.information = information
        self.brand = brand
        self.asin = asin
        self.sku = sku
        self.upc = upc
        self.ean = ean
        self.usd = usd
        self.old_price = old_price
        self.discount_percentage = discount_percentage
        self.discount = discount
        self.earnings_percentage = earnings_percentage
        self.stock = stock
        self.weight = weight
        self.height = height
        self.length = length
        self.width = width
        self.size = size
        self.type = type
        self.product_group = product_group
        self.genre = genre
        self.model = model
        self.edition = edition
        self.platform = platform
        self.format = format
        self.is_prime = is_prime
        self.is_adult_product = is_adult_product
        self.package_height = package_height
        self.package_length = package_length
        self.package_width = package_width
        self.package_weight = package_weight
        self.package_quantity = package_quantity
        self.features = features
        self.category_tree = category_tree
        self.status = status
        self.productType = productType
        self.lastUpdate = lastupdate 
        self.lastRatingUpdate = lastratingupdate
        self.lastPriceChange = lastpricechange 
        self.availabilityAmazon = availabilityamazon 
        self.csv = csv
    def __repr__(self):
        return f'Producto({self.title})'
    def __str__(self):
        return self.title